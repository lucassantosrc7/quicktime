﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	[SerializeField]
	SceneConfig[] sceneConfigs;

	[SerializeField]
	Option [] options;

	void Start()
    {
		for(int i = 0; i < sceneConfigs.Length; i++)
		{
			sceneConfigs[i].sceneInstated = Instantiate(sceneConfigs[i].scene);
			sceneConfigs[i].sceneInstated.SetActive(false);
		}

		ChangeScene(null, sceneConfigs[0]);
	}

	void ChangeScene(SceneConfig currentScene, SceneConfig newScene)
	{
		if (currentScene != null)
		{
			currentScene.sceneInstated.SetActive(false);
		}

		if (newScene == null)
		{
			return;
		}

		for (int i = 0; i < options.Length; i++)
		{
			Option op = options[i];
			op.text.gameObject.SetActive(false);
			op.button.gameObject.SetActive(false);
		}
	
		if (newScene.goToNextScene)
		{
			StartCoroutine(LoadNewScene(newScene, newScene.nextScene));
			return;
		}

		newScene.sceneInstated.SetActive(true);

		Answer [] answers = newScene.answers;

		for (int i = 0; i < answers.Length; i++)
		{
			if (i < options.Length)
			{
				Option op = options[i];
				op.button.onClick.RemoveAllListeners();
				SceneConfig nextScene = answers[i].nextScene;
				op.button.onClick.AddListener(() => NextScene(newScene, nextScene));
				op.text.text = answers[i].text;

				op.text.gameObject.SetActive(true);
				op.button.gameObject.SetActive(true);
			}
		}
	}

	void NextScene(SceneConfig currentScene, SceneConfig newScene)
	{
		ChangeScene(currentScene, newScene);
	}

	IEnumerator LoadNewScene(SceneConfig currentScene, SceneConfig newScene)
	{
		yield return new WaitForSeconds(currentScene.timeToLoad);
		ChangeScene(currentScene, newScene);
	}
}

[System.Serializable]
public class Option
{
	public Button button;
	public Text text;
}
