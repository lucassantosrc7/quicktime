﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SceneConfig", menuName = "CapCorps/SceneConfig", order = 1)]
public class SceneConfig : ScriptableObject
{
	public GameObject scene;
	[HideInInspector]
	public GameObject sceneInstated;

	public Answer [] answers;

	public bool goToNextScene = false;
	[ConditionalHide("goToNextScene", true)]
	public SceneConfig nextScene;

	[ConditionalHide("goToNextScene", true)]
	public float timeToLoad = 2f;
}

[System.Serializable]
public class Answer
{
	public SceneConfig nextScene;
	[TextArea]
	public string text;
}
